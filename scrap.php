<?php

use Recruitment\Scrapper\Fruit as FruitScrapper;
use Recruitment\Collection\Result;

require_once "vendor/autoload.php";

$url = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";

$scrapper = new FruitScrapper( $url );

$result = new Result();

foreach( $scrapper->getItems() as $item){
    $result->addItem( $item );
}


echo json_encode( $result, JSON_PRETTY_PRINT);