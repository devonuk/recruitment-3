<?php

use Recruitment\Scrapper\Fruit;
use Recruitment\Models\Item;

class ScrapperTest extends PHPUnit_Framework_TestCase
{
    protected $fruitScraper;

    public function setUp(){

        $this->fruitScraper = new Fruit( "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html");
    }

    public function testGetData()
    {
        $this->assertTrue( is_array( $this->fruitScraper->getItems() ));
    }

    public function testNotEmptyResponse()
    {
        $this->assertTrue( count( $this->fruitScraper->getItems() ) > 0);
    }

    public function testItemsObjects()
    {
        $item = $this->fruitScraper->getItems();

        $this->assertTrue( $item[0] instanceof Item );
    }
}
