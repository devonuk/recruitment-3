<?php

use Recruitment\Collection\Result;
use Recruitment\Models\Item;

class ResultTest extends PHPUnit_Framework_TestCase
{
    private function getFruitMock()
    {
        $fruitMock = $this->getMock("\Recruitment\Interfaces\Scrapper", array("getItems"));
        $fruitMock->expects($this->any())
            ->method("getItems")
            ->will($this->returnCallback(function () {

                $data = [];
                $data[] = new Item('Banana', '90kb', 2.50, 'Amazing Banana 2 for 1');
                $data[] = new Item('Avocado', '20kb', 1.50, 'Fantastic Avocado 3 for 2');

                return $data;
            }));

        return $fruitMock;
    }

    /**
     * Test add Item to the Result
     */
    public function testAddItemToResult()
    {
        $fruit = $this->getFruitMock();

        $result = new Result();

        foreach( $fruit->getItems() as $item){
            $result->addItem($item);
        }

        $sum = $result->getTotalSum();
        $this->assertEquals($sum, 4);
    }

    /**
     * Test add Item to the Result
     */
    public function testJsonSerialize()
    {
        $fruit = $this->getFruitMock();

        $result = new Result();

        foreach( $fruit->getItems() as $item){
            $result->addItem($item);
        }

        $items = $result->jsonSerialize();

        $this->assertTrue( array_key_exists('results', $items));
        $this->assertTrue( array_key_exists('total', $items));
    }

    /**
     * Test add Item to the Result
     */
    public function testGetItems()
    {
        $fruit = $this->getFruitMock();

        $result = new Result();

        foreach( $fruit->getItems() as $item){
            $result->addItem($item);
        }

        $items = $result->getItems();
        $this->assertTrue($items[0] instanceof Item);
    }
}