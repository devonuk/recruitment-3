<?php

use Recruitment\Models\Item;

class ItemTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Item
     */
    private $item;

    protected function setUp(){

        $this->item = new Item('Banana', '90kb', 2.50, 'Amazing Banana 2 for 1');
    }

    public function testGetItemTitle()
    {
        $this->assertEquals( $this->item->getTitle(), 'Banana');
    }

    public function testGetItemSize()
    {
        $this->assertEquals( $this->item->getSize(), '90kb');
    }

    public function testGetItemUnitPrice()
    {
        $this->assertEquals( $this->item->getUnitPrice(), 2.50);
    }

    public function testGetItemDescription()
    {
        $this->assertEquals( $this->item->getDescription(), 'Amazing Banana 2 for 1');
    }

}