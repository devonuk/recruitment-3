<?php

namespace Recruitment\Scrapper;

use DOMDocument;
use DOMXPath;
use Recruitment\Models\Item as ItemModel;
use Recruitment\Interfaces\Scrapper as ScrapperInterface;

/**
 * Class Fruit
 * @package Recruitm    ent\Scrapper
 * @author Daniel Kurylo <dkurylo1@gmail.com>
 */
class Fruit implements ScrapperInterface
{
    private $url;

    public function __construct( $url ){

        $this->url = $url;
    }

    private function getDetailedData($link)
    {
        $dom = new DOMDocument();

        @$dom->loadHTMLFile( $link );
        $size = strlen($dom->textContent);

        $xpath = new DOMXPath($dom);

        $description = trim($xpath->query('//*[@id="information"]/productcontent/htmlcontent/div[1]/p[1]')->item(0)->nodeValue);

        return array(
            'size' => round(($size / 1024), 2) . "kb",
            'description' => $description
        );
    }

    public function getItems()
    {
        $dom = new DOMDocument();

        @$dom->loadHTMLFile( $this->url );

        $xpath = new DOMXPath($dom);

        $items = [];

        foreach ($xpath->query('//*[@id="productLister"]/ul/li') as $tr) {

            $link = $xpath->query('.//div[*]/div/div/div/h3/a/@href', $tr)->item(0)->nodeValue;

            $details = $this->getDetailedData($link);

            $title = $xpath->query('.//div[*]/div/div/div/h3/a', $tr)->item(0)->nodeValue;
            $title = trim($title);

            $price = $xpath->query('.//p[*]/text()', $tr)->item(0)->nodeValue;

            $items[] = new ItemModel(
                    $title,
                    $details['size'],
                    (float) str_replace('&pound', '', $price),
                    $details['description']
            );
        }

        return $items;
    }
}

