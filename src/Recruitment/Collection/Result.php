<?php

namespace Recruitment\Collection;

use Recruitment\Interfaces\Item as ItemInterface;

/**
 * Class Result
 * @package Recruitment\Collection
 * @author Daniel Kurylo <dkurylo1@gmail.com>
 */
class Result implements \JsonSerializable
{
    private $total = 0;
    private $items = array();

    /**
     * Add Item to the collection
     *
     * @param ItemInterface $i
     */
    public function addItem(ItemInterface $i)
    {
        $this->items[] = $i;
        $this->total += $i->getUnitPrice();
    }

    /**
     * Return data for json serializer
     * @return array
     */
    public function jsonSerialize()
    {
        return array(
            'results' => $this->items,
            'total' => $this->total
        );
    }

    /**
     * Get result total value
     * @return int
     */

    public function getTotalSum()
    {
        return $this->total;
    }

    /**
     * Get collection items
     *
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }
}