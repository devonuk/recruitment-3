<?php

namespace Recruitment\Models;

use Recruitment\Interfaces\Item as ItemInterface;

/**
 * Item model
 *
 * @author Daniel Kurylo
 * @package Recruitment
 */
class Item implements \JsonSerializable, ItemInterface
{
    private $title;

    private $size;

    private $unit_price;

    private $description;

    public function __construct($title, $size, $unit_price, $description)
    {
        $this->title = $title;
        $this->size = $size;
        $this->unit_price = $unit_price;
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->unit_price;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Return data for json serializer
     * @return array
     */
    public function jsonSerialize() {

        return array(
            'title' => $this->description,
            'size' => $this->size,
            'unit_price' => $this->unit_price,
            'description' => $this->description
        );
    }
}