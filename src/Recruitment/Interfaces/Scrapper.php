<?php

namespace Recruitment\Interfaces;

/**
 * Interface Scrapper
 * @package Recruitment\Interfaces
 * @author Daniel Kurylo <dkurylo1@gmail.com>
 */
Interface Scrapper{
    public function getItems();

}