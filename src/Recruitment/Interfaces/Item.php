<?php

namespace Recruitment\Interfaces;

/**
 * Interface ItemInterface
 * @package Recruitment
 *
 * @author Daniel Kurylo
 */

interface Item{

    public function getTitle();

    public function getSize();

    public function getUnitPrice();

    public function getDescription();

}